

class Main


  def Create()
    @lines = Array.new
  end


  def Read()

    File.open("data.txt", "r") do |file|

      testCases= file.readline()

      for line in file.readlines()
        @lines << line
      end
    end
  end


  def Count()

    for line in @lines
      values= line.split(" ")

      neighbours = values[0].to_i
      neighbours= neighbours -1
      values.shift()
      sorted_ary = values.sort.reverse

      sum = 0
      i = 0
      loop do
        if i + 1 > neighbours
          break
        end

        sum= sum + (sorted_ary[i].to_i - sorted_ary[i+1].to_i)
        i=i+1
      end

      puts sum
    end
  end

  
end

main= Main.new
main.Create()
main.Read()
main.Count()
